import React from 'react';
import '../../css/components/image_icon.css';

const getWeatherIcon = (props) => {
  if(props.weatherIcon) {
    var icon = 'http://openweathermap.org/img/w/' + props.weatherIcon + '.png'
    return <img src={icon} alt={props.weatherDescription}/>
  }
}

const getWeather = (props) => {
  return <span>{props.curTemp}&deg;c</span>;
}

const ImageIcon = props => {
  return(
    <div className='weather-image-wrapper'>
      <div className='weather-box-image'>
        { getWeatherIcon(props) }
      </div>
      <div className='weather-details'>
        <div className='weather-temp-max'>{props.curTemp ? getWeather(props) : '' }</div>
      </div>
    </div>
  )
}

export default ImageIcon;