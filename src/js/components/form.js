import React from 'react';
import '../../css/components/form.css';

const Form = props => {
  return(
    <form onSubmit={props.getWeather} autoComplete='off'>
      <input name='city' placeholder='City'/>
      <input name='country' placeholder='Country'/>
      <button className='submit-btn'>></button>
    </form>
  )
}

export default Form;