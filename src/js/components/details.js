import React from 'react';
import '../../css/components/details.css';

//custom methods
const checkForError = (props) => {
  var error = props.error;
  if(error) {
    return error;
  }
  else {
    return (
      <div className='weather-details-wrapper'>
        { props.humidity && <div>Humidity: {props.humidity}%</div> }
        { props.min_temp && <div>Min Temp: {props.min_temp}&deg;c</div> }
        { props.max_temp && <div>Max Temp: {props.max_temp}&deg;c</div> }
        { props.general_description && <div>Description: {props.general_description}</div> }
        { props.detail_description && <div>Description Details: {props.detail_description}</div> }
      </div>
    )
  }
}

const Details = props => {
  return (
    <div className='details-wrapper'>
      { checkForError(props.weatherDetails) }
    </div>
  )
}

export default Details;