import React from 'react';
import '../../css/components/title.css';

const Title = props => {
  return (
    <div className='title-wrapper'>
      <div className='title'>{props.name}</div>
      <div className='sub-text'>{props.subtitle}</div>
    </div>
  );
};

export default Title;