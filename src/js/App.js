import React, { Component } from 'react';
import '../css/main.css';
//import custom components here
import Title from './components/title';
import Form from './components/form';
import Details from './components/details';
import ImageIcon from './components/image_icon';

const API_ID = '43df42134e86e6c4044009e4bbc433f7';

//remeber this is component that consists of a state so we will need to make a class instance
//stateless components just need the React import and the constant method to return JSX
//passing props just requires the 'props' argument for stateless components

//the class is made using just component a i destructured it from the import above
// i.e import React, { Component } from 'react';

class App extends Component {
  state = {
    humidity: undefined,
    cur_temp: undefined,
    min_temp: undefined,
    max_temp: undefined,
    general_description: undefined,
    detail_description: undefined,
    error: undefined
  }
  //custom methods, use arrow function to allow no constructor and auto bind of this
  getWeather = async (e) => {
    //prevent default behavior which will be invoked on form submission
    //and refresh the screen
    e.preventDefault();
    //get the form element and the form inputs and their values
    const city = e.target.city.value, country = e.target.country.value;
    const api_call = await fetch('http://api.openweathermap.org/data/2.5/weather?q=' + city + ',' + country + '&appid=' + API_ID + '&units=metric');
    const dataJSON = await api_call.json();
    
    //validate to make sure user typed something in
    if(city && country) {
      //check the data has values that are returned, fallback silently
      if(dataJSON.main) {
        this.setState({
          humidity: dataJSON.main.humidity,
          cur_temp: dataJSON.main.temp,
          min_temp: dataJSON.main.temp_min,
          max_temp: dataJSON.main.temp_max,
          general_description: dataJSON.weather[0].main,
          detail_description: dataJSON.weather[0].description,
          icon: dataJSON.weather[0].icon,
          error: dataJSON.message !== undefined ? dataJSON.message : undefined
        }) 
      } 
      else {
        this.setState({error: dataJSON.message})
      }
    }
  }

  render() {
    return (
      <div className='weather-wrapper card card-shadow'>
        <ImageIcon weatherIcon={this.state.icon} 
          weatherDescription={this.state.detail_description}
          curTemp={this.state.cur_temp} />
        <div className='weather-box-details'>
          <Title name='Weather App' subtitle='find weather in current location'/>
          <Form getWeather={this.getWeather}/>
          <Details weatherDetails={this.state}/>
        </div>
      </div>
    )
  };
};

export default App;
